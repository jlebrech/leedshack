require 'RMagick'
include Magick

canvas = Magick::Image.new(1000, 1000,
                           Magick::HatchFill.new('white','lightcyan2'))
gc = Magick::Draw.new

#
# galaxy.rb
# Eric Rollins 2008
# Generates x y coordinates for stars in spiral galaxy.

NUM_STARS = 200000

# logarithmic spiral constants 
# http://en.wikipedia.org/wiki/Logarithmic_spiral
A = 1.0
B = 0.20
WINDINGS = 12.4
T_MAX = 2.0 * Math::PI * WINDINGS
# How far stars may be away from spiral arm centers.
DRIFT = 0.3

# Seed random number generator (for repeatabilty).
srand 1234

x_offset = 500
y_offset = 500

for s in 1..NUM_STARS
  t = T_MAX * rand
  x = A * Math.exp(B * t) * Math.cos(t)
  x = x + (DRIFT*x*rand) - (DRIFT*x*rand)
  y = A * Math.exp(B * t) * Math.sin(t)
  y = y + (DRIFT*y*rand) - (DRIFT*y*rand)

  x = x - x_offset
  y = y - y_offset

  # 2 spiral arms
  if rand > 0.5
    gc.point(x, y)
  else
    gc.point(-x, -y)
  end
end

gc.draw(canvas)
canvas.write('galaxy.gif')
