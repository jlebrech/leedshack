require './perlin'
require 'chunky_png'
require 'sinatra'
require 'json'

class Universe
  def initialize
		@p1 = Perlin.new(Random.rand(500), 5, 4)
		@p2 = Perlin.new(Random.rand(500), 5, 3)
		@p3 = Perlin.new(Random.rand(500), 5, 3)

		@buffer = {}
	end

	def get_x_y x, y
		unless @buffer["#{x}_#{y}"].nil? 
			buf = @buffer["#{x}_#{y}"]
			randm = {a:buf[0], b:buf[1]}
			# puts "coord found in buffer"
		else
			randm = randomize(x, y)
			# puts "coord not found in buffer"
		end

		{x:x,y:y,a:randm[:a],b:randm[:b]}
	end

	private

  def randomize x, y
		a = @p1.run(x, y) + @p1.run(y, x)+ @p3.run(y, x) + 100
		b = @p2.run(x, y) + @p2.run(y, x)+ @p3.run(x, y) + 100

		# if a > 300
			# puts "found a star"
	    @buffer["#{x}_#{y}"] = [a, b]
	  # else
	  # 	puts "found empty space"
	  #   @buffer["#{x}_#{y}"] = [0, 0]
	  #   a, b = [0,0]
	  # end

	  {a:a,b:b}
	end
end

universe = Universe.new

set :public_folder, 'public'

get "/:x/:y" do
  png = ChunkyPNG::Image.new(100, 100, ChunkyPNG::Color::BLACK)

  px = params[:x].to_i
  py = params[:y].to_i

  for x in px..px+99
    for y in py..py+99
      location = universe.get_x_y(x, y)

      if location[:a] > 200
        png[x-px,y-py] = ChunkyPNG::Color.rgba( location[:a].to_i, 60, 60, location[:b].to_i)
      end
    end
  end

  content_type 'image/png'
  png.to_blob
end

get '/' do
  File.read(File.join('public', 'index.html'))
end
