class RandomGalaxy

  def initialize
    @number_of_stars_in_core = 1000
    @number_of_stars_in_disk = 4000
    @disk_radius = 70
    @hub_radius = 45
    @number_of_arms = 4
    @arm_tightness = 1
    @arm_width = 50
    @fuzz = 20

    @size = 80

    @arm_separation = 360.0 / @number_of_arms

    @stars = Array.new
    @maxim = 0
  end

  def generate_galaxy
    [0..(@number_of_stars_in_core + @number_of_stars_in_disk)].each do |i|
      @stars << {x:0, y:0}
    end

    # do arms
    for i in [0..@number_of_stars_in_disk]
      dist = @hub_radius + Random.rand * @disk_radius

      angle = ((360 * @arm_tightness * (dist / @disk_radius)) +
           Random.rand * @fuzz * 2.0 - @fuzz)
      @stars[i][:x] = cosd(angle) * dist
      @stars[i][:y] = sind(angle) * dist
      @maxim = [@maxim, dist].max
    end

    # do hub
    for i in [@number_of_stars_in_disk..(@number_of_stars_in_core+@number_of_stars_in_disk)]
      dist = Random.rand * @hub_radius
      angle = Random.rand * 360
      @stars[i][:x] = cosd(angle) * dist
      @stars[i][:y] = sind(angle) * dist
      @maxim = [@maxim, dist].max
    end

    factor = @size / (maxim * 2)

    # fit galaxy into buffer
    for i in [0..(@number_of_stars_in_disk+@number_of_stars_in_core)]
      sx = map_range(@stars[i].x -@maxim, @maxim, 0, @size-1).floor
      sy = map_range(@stars[i].y -@maxim, @maxim, 0, @size-1).floor
      buffer[sy][sx] = [255, buffer[sy][sx].floor + 1].min
    end
  end

  def cosd degrees
    Math.cos(degrees * 0.017453292519943295)
  end

  def sind degrees
    Math.sin(degrees * 0.017453292519943295)
  end

  def map_range x, in_min, in_max, out_min, out_max
    (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
  end

end

RandomGalaxy.new.generate_galaxy
